####################################################################################################
# CONVERSION LOGIC SETUP.PY TEMPLATE                                                               #
# 0.3: Add 'publish' command for publishing binary wheels in one step                              #
# 0.2: Generate package lists from requirements.txt file; add resource files                       #
# 0.1: Initial version - standardize version numbering based on Bamboo variables                   #
####################################################################################################

from __future__ import print_function, absolute_import, division, unicode_literals
import getpass, datetime, re, setuptools, os, sys, subprocess

# Package Metadata (customize for each package)
NAME = 'tests'
VERSION = '0.1'
DESCRIPTION = 'TestRail extension'
ENTRY_POINTS = {
    'console_scripts': [
        'automation = cl.tests.test_rail:run_main',
    ]
}

# CLSetup parameters (customize if needed)
RESOURCE_PATTERN = 'resource'
INSTALL_REQUIRES = None  # auto-detect from requirements.txt
WRITE_MANIFEST = True  # auto-build Manifest.in file
DEV_ONLY_REQUIREMENTS = ['pyhamcrest', 'mock', 'selenium']
PIP_REPOSITORY = 'conversionlogic'


####################################################################################################
class CLSetup(object):
    """
    Wrapper around setuptools setup.py to standardize version numbering from Bamboo environment variables.
    """
    def __init__(self, name, base_version, build=None, branch=None):
        self._name = name
        self._base_version = base_version
        self._build = build or CLSetup.getenv('bamboo_buildNumber')
        self._branch = branch or CLSetup.getenv('bamboo_planRepository_branchName')
        self._version = CLSetup.normalize_version(self._base_version, self._branch, self._build)

    def setup(self, **kwargs):
        if len(sys.argv) >= 2 and sys.argv[1] == 'artifact':
            print(self.artifact)

        elif sys.argv[1] == 'publish':
            packages = kwargs.pop('packages', setuptools.find_packages())
            setuptools.setup(name=self.name, version=self.version, packages=packages, script_args=['bdist_wheel'], **kwargs)
            subprocess.check_call('twine upload -r {} {}'.format(PIP_REPOSITORY, self.wheel_pattern), shell=True)

        else:
            packages = kwargs.pop('packages', setuptools.find_packages())
            setuptools.setup(name=self.name, version=self.version, packages=packages, **kwargs)

    @property
    def name(self):
        return self._name

    @property
    def version(self):
        # master: VERSION.BUILD
        # branch: VERSION.0.BUILD.dev<BRANCH>
        # local:  VERSION.0.TIMESTAMP+USER
        return self._version

    @property
    def artifact(self):
        return 'dist/{}-{}.tar.gz'.format(self.name, self.version)

    @property
    def wheel_pattern(self):
        return 'dist/{}-{}-*.whl'.format(self.name.replace('-', '_'), self.version)

    @staticmethod
    def normalize_version(version, branch, build, pattern=re.compile('[A-Za-z]+-([0-9]+)')):
        if branch:
            if branch == 'master':
                return '{}.{}'.format(version, build)

            match = pattern.match(branch)
            if match:
                # DA-XXXX branch
                return '{}.0.{}.dev{}'.format(version, build, match.groups()[0])

            # non-DA-XXXX branch
            return '{}.0.{}+{}'.format(version, build, re.sub('\W', '', branch))

        else:
            # no branch
            return '{}.0.{}+{}'.format(version, datetime.datetime.utcnow().strftime('%Y%m%d%H%M%S'), getpass.getuser())

    @staticmethod
    def getenv(key):
        return os.environ.get(key, None)

    @staticmethod
    def write_manifest():
        with open('MANIFEST.in', 'wt') as w:
            w.write('recursive-include */{} *\n'.format(RESOURCE_PATTERN).encode('utf-8'))
            w.write('include requirements.txt\n'.encode('utf-8'))
            w.write('include cl/lib/api_wrapper/config.yml\n'.encode('utf-8'))

    @staticmethod
    def build_requirements(ignore=DEV_ONLY_REQUIREMENTS):
        def ok(package):
            return len(package) > 0 and not package.startswith('-') and package not in ignore

        with open('requirements.txt', 'rt') as reader:
            return [l for l in [ll.strip() for ll in reader] if ok(l)]


if __name__ == '__main__':
    if WRITE_MANIFEST:
        CLSetup.write_manifest()
    install_requires = CLSetup.build_requirements() if INSTALL_REQUIRES is None else INSTALL_REQUIRES
    CLSetup(NAME, VERSION).setup(description=DESCRIPTION,
                                 install_requires=install_requires,
                                 entry_points=ENTRY_POINTS,
                                 include_package_data=True)