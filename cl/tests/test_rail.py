# -*- coding: utf-8 -*-
'''
Created on Feb 15, 2017

@author: ekhim
'''
from cl.lib.api_wrapper.testrail import APIClient

import requests, jsonpath, json, urlparse, time, threading, traceback
from multiprocessing import Process
from datetime import datetime

from cl.features.steps.consumer import *

from behave import model, parser, runner, step_registry
from mock import Mock, patch
from nose.tools import *
import re
from _elementtree import ParseError
from django.contrib.gis.db.backends.spatialite import client
from django.utils.encoding import smart_str, smart_unicode

from cl_cli import cli, run_main
from cl_config import ConfigLoader

class Test:
    def __init__(self, env=None, dry_run=True, env_config=None):
        print "."

    def setUp(self):
        """Runs at the beginning of each run"""
        print "setup"
#         self.context = Browser()
# #       #configure context behave.runner.Context.execute_steps() functionality.
#         runner_ = Mock()
#         self.config = runner_.config = Mock()
#         runner_.config.verbose = False
#         runner_.config.stdout_capture  = False
#         runner_.config.stderr_capture  = False
#         runner_.config.log_capture  = False
#         
#         self.context = runner.Context(runner_)
#         # self.context.text = None
#         # self.context.table = None
#         runner_.context = self.context
#         self.context.feature = Mock()
#         self.context.feature.parser = parser.Parser()

    def tearDown(self):
        """Runs at the end of each run"""
        print "teardown"
        pass
#         try:
#             self.context = Browser()
#             self.context.close()
#         except WebDriverException: 
#             print "Couldn't close Driver"

    def run(self):
        client = APIClient("https://conversionlogic.testrail.com/")
        client.user = 'ekhim@sqasquared.com'
        client.password = '!Letmein1!'

#         project_ids = self.extract_project(client)
        project_ids = [3]

        for project_id in project_ids:
            self.execte_project(client, project_id)

#         jobs = []
#         for project_id in project_ids:
#             self.execte_project(client, project_id)
#              
#             process = Process(target=self.execte_project, 
#                                               args=(client, project_id))
#             jobs.append(process)
#          
#         # Start the processes (i.e. calculate the random number lists)        
#         for j in jobs:
#             j.start()
#         # Ensure all of the processes have finished
#         for j in jobs:
#             j.join()
        
    def execte_project(self, client, project_id):
            plans = client.send_get("get_plans/" + str(project_id))
            for plan in plans: #plan JSON object
                #get config_id in plan object
                plan_id = jsonpath.jsonpath(plan, '$..id')
                #extract run_ids if configuration is 'Automation'
                runs = jsonpath.jsonpath(self.extract_plan(client, plan_id[0]), '$..runs[*]')
                #check if configuration exist before executing run
                config_exists = jsonpath.jsonpath(self.extract_plan(client, plan_id[0]), '$..config')
                print self.extract_plan(client, plan_id[0])
                print config_exists
                if config_exists is False or all(x is None for x in config_exists) or any('Automation' not in x for x in config_exists):
                    print "No configuration exist for run in plan"
                    continue
                
                jobs = []
                for run in runs:
                    process = Process(target=self.execute_run, 
                                                      args=(client, run, project_id))
                    jobs.append(process)
                 
                # Start the processes (i.e. calculate the random number lists)        
                for j in jobs:
                    j.start()
                # Ensure all of the processes have finished
                for j in jobs:
                    j.join()
                
    def execute_run(self, client, run, project_id):
                    #       #configure context behave.runner.Context.execute_steps() functionality.
                    runner_ = Mock()
                    self.config = runner_.config = Mock()
                    runner_.config.verbose = False
                    runner_.config.stdout_capture  = False
                    runner_.config.stderr_capture  = False
                    runner_.config.log_capture  = False
                    
                    self.context = runner.Context(runner_)
                    # self.context.text = None
                    # self.context.table = None
                    runner_.context = self.context
                    self.context.feature = Mock()
                    self.context.feature.parser = parser.Parser()
                #check per run
#                 for run in runs: #run JSON object
                    print run
                    browser = self.extract_browsers(client, project_id, run)[0]
                    #extract config_id to determine if plan includes 'Automation' execution type
                    config_ids = jsonpath.jsonpath(run, '$..config_ids')
                    if any(isinstance(i, list) for i in config_ids):
                        #convert nested list to single list
                        config_ids = [val for sublist in config_ids for val in sublist]
                    if config_ids is not None:
                        for config_id in config_ids: #config_id
                            #get group_id by config_id
                            print self.extract_configs(client, project_id)
                            group_ids = jsonpath.jsonpath(self.extract_configs(client, project_id), '$..configs[?(@.id=={id})].group_id'.format(id=config_id))
                            if any(isinstance(i, list) for i in group_ids):
                                #convert nested list to single list
                                group_ids = [val for sublist in group_ids for val in sublist]
                            #check if execution type is automation per run
                            for group_id in group_ids:
                                group_name = jsonpath.jsonpath(self.extract_configs(client, project_id), '$..[?(@.id=={id})].name'.format(id=group_id))
                                if 'Execution Type' in group_name:
                                    config_name = jsonpath.jsonpath(self.extract_configs(client, project_id), '$..configs[?(@.id=={id})].name'.format(id=config_id))
                                    if 'Automation' in config_name:
                                        run_ids = jsonpath.jsonpath(run, '$..id')
                                        for run_id in run_ids:
                                            case_ids = jsonpath.jsonpath(self.extract_tests(client, run_id), '$..case_id')
                                            for case_id in case_ids:
                                                self.execute_steps(client, case_id, run_id, browser)
        
    def extract_project(self, client):
        return jsonpath.jsonpath(client.send_get("get_projects"), '$..id')
    
    def extract_configs(self, client, project_id):
        return client.send_get("get_configs/" + str(project_id))

    def extract_run(self, client, run_id):
        return client.send_get("get_run/" + str(run_id))#str(project_id))
    
    def extract_plan(self, client, plan_id):
        return client.send_get("get_plan/" + str(plan_id))
    
    def extract_tests(self, client, run_id):
        return client.send_get("get_tests/" + str(run_id))
    
    def extract_browsers(self, client, project_id, run):
        browsers = []
        #extract config_id to determine if plan includes 'Automation' execution type
        config_ids = jsonpath.jsonpath(run, '$..config_ids')
        if any(isinstance(i, list) for i in config_ids):
            #convert nested list to single list
            config_ids = [val for sublist in config_ids for val in sublist]
        if config_ids is not None:
            for config_id in config_ids: #config_id
                #get group_id by config_id
                group_ids = jsonpath.jsonpath(self.extract_configs(client, project_id), '$..configs[?(@.id=={id})].group_id'.format(id=config_id))
                if any(isinstance(i, list) for i in group_ids):
                    #convert nested list to single list
                    group_ids = [val for sublist in group_ids for val in sublist]
                #check if Browser is automation per run
                for group_id in group_ids:
                    group_name = jsonpath.jsonpath(self.extract_configs(client, project_id), '$..[?(@.id=={id})].name'.format(id=group_id))
                    if 'Browser' in group_name:
                        browsers.append(jsonpath.jsonpath(self.extract_configs(client, project_id), '$..configs[?(@.id=={id})].name'.format(id=config_id)))
        if any(isinstance(i, list) for i in browsers):
            #convert nested list to single list
            browsers = [val for sublist in browsers for val in sublist]
        if not browsers:
            browsers.append('htmlunit')
                           
        return browsers
    
    def execute_steps(self, client, case_id, run_id, browser):
        steps = []
        if any(x is not None for x in jsonpath.jsonpath(client.send_get("get_case/" + str(case_id)), '$..custom_preconds')):
            steps.append(''.join(jsonpath.jsonpath(client.send_get("get_case/" + str(case_id)), '$..custom_preconds')) + "\r\n")
        if any(x is not None for x in jsonpath.jsonpath(client.send_get("get_case/" + str(case_id)), '$..custom_steps')):
            steps.append(''.join(jsonpath.jsonpath(client.send_get("get_case/" + str(case_id)), '$..custom_steps')) + "\r\n")
        if any(x is not None for x in jsonpath.jsonpath(client.send_get("get_case/" + str(case_id)), '$..custom_expected')):
            steps.append(''.join(jsonpath.jsonpath(client.send_get("get_case/" + str(case_id)), '$..custom_expected')) + "\r\n")
            
        if steps:
            steps.insert(0, "given launch %s browser\r\n" % browser)
            steps.append("given teardown\r\n")
            
            print re.sub('\d+[\.]', 'given', ''.join(steps))
            
            start = datetime.now()
            try:
                step_consumer(self.context, re.sub('\d+[\.]', 'given', ''.join(steps)))
                #update test case for passed scenario
                done = datetime.now()
                elapsed = done - start
                self.update_case(client, case_id, run_id, True, "Executed by automated TestRail framework: passed", elapsed.seconds)
            except AssertionError, e:  # -- PY26-CLEANUP-MARK
                #update test case for failed scenario
                traceback.print_exc()
                done = datetime.now()
                elapsed = done - start
                self.update_case(client, case_id, run_id, False, smart_str(e), elapsed.seconds)
            except Exception as e:
                done = datetime.now()
                elapsed = done - start
                self.update_case(client, case_id, run_id, 4, str(e) + "\nCheck your steps provides number line. Ex. '1. Go to this page'", elapsed.seconds)

    def update_case(self, client, case_id, run_id, result_flag, msg, elapsed):
        #Update the result in TestRail using send_post function. 
        #Parameters for add_result_for_case is the combination of runid and case id. 
        #status_id is 1 for Passed, 2 For Blocked, 4 for Retest and 5 for Failed

        if result_flag is True:
            status_id = 1
        elif result_flag is False:
            status_id = 5
        else:
            status_id = result_flag

        elapsed = str(0) if elapsed == 0 else str(elapsed) + 's'

        client.send_post(
                'add_result_for_case/%s/%s'%(run_id, case_id),
                {'status_id': status_id, 'comment': msg, 'elapsed': elapsed, 'version': 'SQA2', 'assignedto_id': 8 })


@cli.command('run')
def run(args, runner, **kwargs):
    runner.run()
 
 
@cli.main
@cli.arg('-e', '--env',           help='Environment name (default: from meta)')
@cli.arg('-c', '--env_config',    help='Environment config file (e.g. config/dev.yml)')
@cli.arg('-v', '--verbose',       action='store_true', default=False, help='Enable verbose logging')
@cli.arg('-q', '--quiet',         action='store_true', default=False, help='Enable quiet logging')
@cli.arg('-n', '--dryrun',        action='store_true', default=False, help='Dry Run (default: False)')
@cli.arg('-o', '--output',        help='Output file (default: stdout)')
@cli.arg('action')
def main(args, extra_args):
    cli.init_logging(args.verbose, args.quiet)
    runner = Test(args.env, args.dryrun, args.env_config)
    cli.run(args.action, extra_args, runner=runner)

 
if __name__ == '__main__':
    run_main()

# if __name__ == '__main__':
#     unittest.main(verbosity=2)