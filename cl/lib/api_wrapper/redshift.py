from __future__ import print_function, absolute_import, division, unicode_literals

import unittest, yaml, logging, sys, os, getpass, datetime, smart_open
from cl_redshift.redshift import RedshiftClient, RedshiftCursor, Create, Query, Delete, Unload, Insert, Update, Append
from hamcrest import *

from db import DB
LOG = logging.getLogger(__file__)
logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(module)s: %(msg)s',
                    stream=sys.stdout)
logging.getLogger('botocore').setLevel(logging.ERROR)
logging.getLogger('boto3').setLevel(logging.ERROR)

def to_datetime(s): return datetime.datetime.strptime(s, '%Y-%m-%d')
def to_date(s): return datetime.datetime.strptime(s, '%Y-%m-%d').date()

class BaseClientTest:
    """Base class for RedshiftClient integration tests - configures and creates a live connection"""
    def open_connection(self):
#     def setUp(self):
        LOG.info('setup red')
#         super(BaseClientTest, self).setUp()
        path = os.path.join(os.path.dirname(__file__), 'config.yml')
        LOG.info('Loading integration-test configuration: {}'.format(path))
        with open(path, 'rU') as f:
            config = yaml.load(f)
            self.client = RedshiftClient(user_namespace=True, **config['redshift'])
        self.connection = self.client.connect()
    
    def close_connection(self):
#     def tearDown(self):
        LOG.info('teardown red')
        self.connection.close()
#         super(BaseClientTest, self).tearDown()

    def generate_table_name(self, base_name=None, schema=None):
        schema = schema or self.client.schema
        base_name = base_name or 'rsutil_{}'.format(datetime.datetime.utcnow().strftime('%Y%m%d_%H%M%S'))
        return '{}.{}_{}'.format(schema, getpass.getuser(), base_name)

    def cursor(self):
        self.open_connection()
        return self.connection.cursor()


class RedshiftClientTest(BaseClientTest):
    def test_cursor(self):
        assert_that(self.client.connect().cursor(), instance_of(RedshiftCursor))


class BaseTableTest(BaseClientTest):
    """Base class for table-driven test cases - creates and cleans up user-namespaced table"""
    TEST_TABLES = []
    TABLE_NAME = None
    COLUMNS = None
    ROWS = None

    def setup(self):
#         super(BaseTableTest, self).setUp()
        if self.TABLE_NAME:
            self.table = self.create_temp_table(self.TABLE_NAME, columns=self.COLUMNS)
            if self.ROWS:
                self.insert(self.table, self.ROWS)

    def tearDown(self):
        with self.cursor() as cursor:
            [ Delete.drop(cursor, t, if_exists=False) for t in self.TEST_TABLES ]
            cursor.commit()
        del self.TEST_TABLES[:]
#         super(BaseTableTest, self).tearDown()

    def create_temp_table(self, name, columns, *args, **kwargs):
        LOG.info('Creating temp table: {}'.format(name))
        table = self.client.table(name)
        with self.cursor() as cursor:
            if Query.exists(cursor, table):
                Delete.drop(cursor, table)
            Create.create(cursor, table, columns=columns, *args, **kwargs)
            cursor.commit()
        self.teardown_table(table)
        return table

    def teardown_table(self, table):
        self.TEST_TABLES.append(table)

    def insert(self, table, rows):
        with self.cursor() as cursor:
            for row in rows:
                values = ','.join([ '%s' for _ in range(len(row)) ])
                cursor.execute('INSERT INTO {} VALUES ({})'.format(table, values), row)
        return table

    def read_unloaded(self, prefix, max_slices=6, max_parts=1):
        result = []
        for slice in range(max_slices):
            for part in range(max_parts):
                try:
                    url = '{pre}{slice:04d}_part_{part:02d}'.format(pre=prefix, slice=slice, part=part)
                    print('reading: {}'.format(url))
                    result.extend([l.strip() for l in smart_open.smart_open(url)])
                except:
                    LOG.exception('err')
        print('Unload result: \n{}'.format(result))
        return result

    def assert_table(self, cursor, table, expected):
        self.assert_rows(Query.select_all(cursor, table).fetchall(), expected)

    def assert_rows(self, actual, expected):
        self.setup()
        def match(row):
            return has_properties(**row) if isinstance(row, dict) else contains(*row)
        assert_that(actual, contains_inanyorder(*[ match(row) for row in expected ]), '\n> Actual response:\n> %s\n' % (actual))
        self.tearDown()

class TestSelect(BaseTableTest):

    def test_select__criteria_list(self, table, columns=None, criteria=None, group=None, order=None, args=None,
               join_on=None, join_using=None, full_on=None, full_using=None,
               right_on=None, right_using=None, left_on=None, left_using=None,
               having=None, limit=None, expected=has_properties):
        with self.cursor() as cursor:
            self.assert_rows(Query.select(cursor, table, columns, criteria, group, order, args, join_on, join_using, full_on, full_using, right_on, right_using, left_on, left_using, having, limit).fetchall(),
                             expected)
        self.close_connection()
            

# if __name__ == "__main__":  
#     suite = unittest.TestLoader().loadTestsFromTestCase(TestSelect)
#     unittest.TextTestRunner(verbosity=3).run(suite)
            