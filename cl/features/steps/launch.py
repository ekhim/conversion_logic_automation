# -*- coding: utf-8 -*-
'''
Created on Feb 15, 2017

@author: ekhim
'''
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from behave import given, when, then

from cl.features.steps.dict import dictionary

@given('launch {browser} browser')
def before_all(context, browser):
    """Runs at the beginning of each test"""
    if 'firefox' in browser.lower():
        context.driver = webdriver.Firefox()
        context.driver.implicitly_wait(10)
    elif 'chrome' in browser.lower():
        context.driver = webdriver.Chrome()
        context.driver.implicitly_wait(10)
    elif 'explore' in browser.lower():
        context.driver = webdriver.Ie()
        context.driver.implicitly_wait(10)

@given('teardown')
def after_all(context):
    """Runs at the end of each test"""
    try:
        context.driver.quit()
    except WebDriverException: 
        print "Couldn't close Driver"

@given('go to {source} at {location}')
def step_impl(context, source, location):
    if 'elastic' not in source:
        context.driver.get(location)
    dictionary[source] = location
