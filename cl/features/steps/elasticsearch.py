# -*- coding: utf-8 -*-
'''
Created on Feb 15, 2017

@author: ekhim
'''
from behave import given, when, then
import requests, jsonpath, json, urlparse, base64

from django.utils.encoding import smart_str
from hamcrest import *

from cl.features.steps.dict import dictionary

@given('verify {id} is {expect} for {query} in elastic search')
def step_impl(context, id, expect, query):
    #query elastic search by GET request
    assert dictionary['elastic search'] is not None, 'ERROR expected host http://elk.conversionlogic.net as a location in SUB-STEP Go to {source} at {locaton} step'
    response = requests.get(dictionary['elastic search'] + "/elasticsearch/_search?sort=@timestamp:desc&q=" + query)
    
    json_objects = json.loads(response.text)["hits"]["hits"]

    #iterate over a single json_object
    for json_object in json_objects:
        if 'null' in expect:
            #validate {id} does not contain null, None, or is not empty in elastic search
            assert_that(jsonpath.jsonpath(json_object, '$..' + id), has_items(is_not('null'),
                                                                is_not(None), 
                                                                is_not(empty())), "\n> %s\n" % (smart_str(json_object)))
        elif 'match' in expect:
            #match query parameters from log against attributes in elastic search
            match = jsonpath.jsonpath(json_object, '$..' + id)
            #parse query parameters and store in object
            query_data = urlparse.parse_qs(match[0])
#             print query_data
            #iterate over _source object in elastic search
            for key, value in query_data.items():
                if 'cx' in key:
                    cx_decoded = base64.b64decode(jsonpath.jsonpath(json_object, '$..cx')[0])
                    assert_that(cx_decoded, contains_string(jsonpath.jsonpath(json_object, '$..cx_unwrapped')[0]), "\n> key:%s\n> %s\n" % (key, smart_str(json_object)))
                    for cx_key, cx_value in json.loads(jsonpath.jsonpath(json_object, '$..cx_unwrapped')[0]).iteritems():
                        #iterate through json object for cx_unwrapped
                        assert_that(jsonpath.jsonpath(json_object, '$..' + cx_key), has_items(contains_string(cx_value)), "\n> key:%s\n> %s\n" % (key, smart_str(json_object)))
                elif 'cl_' in key:
                    #keys with prefix 'cl_' is reflected in refr
                    assert_that(jsonpath.jsonpath(json_object, '$..refr'), has_items(contains_string(key + '=' + value)), "\n> key:%s\n> %s\n" % (key, smart_str(json_object)))
                elif 'tz' in key or 'url' in key:
                    #to encode/decode ??import urllib
                    assert_that(jsonpath.jsonpath(json_object, '$..key'), has_items(value[0].encode('utf-8')), "\n> key:%s\n> %s\n" % (key, smart_str(json_object)))
                else:
                    #validate {id} against keys in elastic search
                    data = jsonpath.jsonpath(json_object, '$..' + key)
                    if any(isinstance(i, list) for i in data):
                        #convert nested list to single list
                        data = [val for sublist in data for val in sublist]
                    print data
                    assert_that(data, has_items(is_in(value)), "\n> key:%s\n> %s\n" % (key, smart_str(json_object)))#, has_items(query_data[key]))

        else:
            #validate {id} against {expected} in elastic search
            data = jsonpath.jsonpath(json_object, '$..' + id)
            if any(isinstance(i, list) for i in data):
                #convert nested list to single list
                data = [val for sublist in data for val in sublist]
            assert_that(data, has_items(contains_string(expect)), "\n> %s\n" % (smart_str(json_object)))
