# -*- coding: utf-8 -*-
'''
Created on Feb 15, 2017

@author: ekhim
'''

from behave import given, when, then
from db import DB
from hamcrest import *

from cl.features.steps.dict import dictionary
from cl.lib.api_wrapper.redshift import TestSelect

@given('verify {expect} for {table} by {criteria} in redshift')
def step_impl(context, expect, table, criteria):
    t = eval('{%s}' % (expect))
    criteria = criteria.split(",")

    context.redshift = TestSelect()

    expected = []
    expected.append(t)

    context.redshift.test_select__criteria_list(table, expected=expected, criteria=criteria, limit=100)
