# -*- coding: utf-8 -*-
'''
Created on Feb 15, 2017

@author: ekhim
'''

import launch, elasticsearch, redshift

def step_consumer(context, text):
    doc = u'''
        {steps}
    '''.lstrip().format(steps=text)

    context.execute_steps(doc)